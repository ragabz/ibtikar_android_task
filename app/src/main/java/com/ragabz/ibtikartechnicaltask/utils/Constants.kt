package com.ragabz.ibtikartechnicaltask.utils

class Constants {
    companion object {

        //region retrofit okhttp times
        const val CONNECT_TIMEOUT: Long = 12 * 1000 // 2 minutes
        const val READ_TIMEOUT: Long = 12 * 1000 // 2 minutes
        const val WRITE_TIMEOUT: Long = 12 * 1000 // 2 minutes
        //endregion

        //region api
        const val APP_KEY_VALUE = "cdb038aa9998174544492ddbe293dd0e"
        const val IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w185/"
        //endregion

    }
}