package com.ragabz.ibtikartechnicaltask.utils

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class Utils {
    companion object {
        fun runAfter(delay:Long, completion:()->Unit){
            Observable.timer(delay, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe {
                completion()
            }
        }
    }
}