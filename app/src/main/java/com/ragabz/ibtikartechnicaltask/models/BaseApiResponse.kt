package com.ragabz.ibtikartechnicaltask.models

import com.google.gson.annotations.SerializedName

data class BaseApiResponse(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val people: List<Person>,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") val totalResults: Int
)