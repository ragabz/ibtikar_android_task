package com.ragabz.ibtikartechnicaltask.models

data class PersonImagesResponse(
    val id: Int,
    val profiles: List<Profile>
)