package com.ragabz.ibtikartechnicaltask.di.modules

import dagger.Module

@Module(includes = [NetworkModule::class])
class ApplicationModule