package com.ragabz.ibtikartechnicaltask.di.modules

import com.ragabz.ibtikartechnicaltask.ui.people.PeopleActivity
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodel.ImageViewModelModule
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodel.PeopleViewModelModule
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodel.PersonDetailsViewModelModule
import com.ragabz.ibtikartechnicaltask.ui.persondetails.PersonDetailsActivity
import com.ragabz.ibtikartechnicaltask.ui.personimage.ImageActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {


    @ContributesAndroidInjector(modules = [PeopleViewModelModule::class])
    abstract fun contributePeopleActivity(): PeopleActivity

    @ContributesAndroidInjector(modules = [PersonDetailsViewModelModule::class])
    abstract fun contributePersonDetailsActivity(): PersonDetailsActivity

    @ContributesAndroidInjector(modules = [ImageViewModelModule::class])
    abstract fun contributeImageActivity(): ImageActivity

}