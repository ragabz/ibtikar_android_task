package com.ragabz.ibtikartechnicaltask.di.components

import android.app.Application
import com.ragabz.ibtikartechnicaltask.BaseApplication
import com.ragabz.ibtikartechnicaltask.di.modules.ActivityBuilderModule
import com.ragabz.ibtikartechnicaltask.di.modules.ApplicationModule
import com.ragabz.ibtikartechnicaltask.di.modules.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        ApplicationModule::class,
        ViewModelFactoryModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<BaseApplication> {

    // overriding the regular builder for component
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

}


