package com.ragabz.ibtikartechnicaltask.di.modules

import androidx.lifecycle.ViewModelProvider
import com.ragabz.ibtikartechnicaltask.common.ViewModelProvidersFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(
        viewModelProvidersFactory: ViewModelProvidersFactory
    ): ViewModelProvider.Factory

}