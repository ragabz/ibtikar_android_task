package com.ragabz.ibtikartechnicaltask.data

import com.ragabz.ibtikartechnicaltask.models.BaseApiResponse
import com.ragabz.ibtikartechnicaltask.models.PersonImagesResponse
import com.ragabz.ibtikartechnicaltask.network.PeopleApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PeopleRepository @Inject constructor(private val peopleApi: PeopleApi) {

    fun fetchPopularPeople(pageNumber: Int): Single<BaseApiResponse> {
        return peopleApi.getPopularPeople(page = pageNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getPersonImages(personId: Int): Single<PersonImagesResponse> {
        return peopleApi.getPersonIMages(personId = personId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}