package com.ragabz.ibtikartechnicaltask.ui.persondetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ragabz.ibtikartechnicaltask.R
import com.ragabz.ibtikartechnicaltask.common.ViewModelProvidersFactory
import com.ragabz.ibtikartechnicaltask.common.base.BaseActivity
import com.ragabz.ibtikartechnicaltask.common.views.ItemClickSupport
import com.ragabz.ibtikartechnicaltask.databinding.ActivityPersonDetailsBinding
import com.ragabz.ibtikartechnicaltask.models.Person
import com.ragabz.ibtikartechnicaltask.ui.people.ItemType
import com.ragabz.ibtikartechnicaltask.ui.persondetails.viewmodels.PersonDetailsViewModel
import com.ragabz.ibtikartechnicaltask.ui.personimage.ImageActivity
import java.io.Serializable
import javax.inject.Inject

class PersonDetailsActivity : BaseActivity() {

    private val TAG = PersonDetailsActivity::class.java.simpleName

    @Inject
    lateinit var providerFactory: ViewModelProvidersFactory

    private val personDetailsViewModel: PersonDetailsViewModel by lazy {
        ViewModelProvider(this, providerFactory).get(PersonDetailsViewModel::class.java)
    }

    private lateinit var binding: ActivityPersonDetailsBinding

    companion object {

        const val EXTRA_PERSON_KEY: String = "com.ragab.ibtikartask.person"

        fun newIntent(context: Context, person: Person): Intent {
            return Intent(context, PersonDetailsActivity::class.java).apply {
                putExtra(EXTRA_PERSON_KEY, person as Serializable)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_person_details)
        binding.lifecycleOwner = this
        getPersonFromIntentToViewModel()
        binding.personDetailsViewModel = personDetailsViewModel

        initLayoutManager()
        initRecyclerViewItemDecoration()
        binding.recyclerViewImages.itemAnimator = DefaultItemAnimator()
        setupClickListenerToRecyclerViewImages()
        personDetailsViewModel.loadPersonImages()


    }

    fun getPersonFromIntentToViewModel() {
        val person: Person = intent.extras?.get(EXTRA_PERSON_KEY) as Person
        personDetailsViewModel.initPerson(person)
    }


    fun initLayoutManager() {
        val layoutManager = GridLayoutManager(this, 2)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                when (binding.recyclerViewImages.adapter?.getItemViewType(position)) {
                    ItemType.LOADING.value -> return 2
                    ItemType.ITEM.value -> return 1
                    else -> return -1
                }
            }
        }
        binding.recyclerViewImages.setLayoutManager(layoutManager)
    }

    fun initRecyclerViewItemDecoration() {
        binding.recyclerViewImages.addItemDecoration(
            DividerItemDecoration(this, RecyclerView.VERTICAL)
        )
    }


    fun setupClickListenerToRecyclerViewImages() {
        ItemClickSupport.addTo(binding.recyclerViewImages).setOnItemClickListener(object :
            ItemClickSupport.OnItemClickListener {
            override fun onItemClicked(recyclerView: RecyclerView, position: Int, v: View) {
                startActivity(
                    ImageActivity.newIntent(
                        this@PersonDetailsActivity,
                        personDetailsViewModel.getProfileAtIndex(position)
                    )
                )
            }
        })
    }
}
