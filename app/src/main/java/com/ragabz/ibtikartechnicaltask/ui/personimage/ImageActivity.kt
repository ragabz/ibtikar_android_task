package com.ragabz.ibtikartechnicaltask.ui.personimage

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.ragabz.ibtikartechnicaltask.common.ViewModelProvidersFactory
import com.ragabz.ibtikartechnicaltask.common.base.BaseActivity
import com.ragabz.ibtikartechnicaltask.databinding.ActivityImageBinding
import com.ragabz.ibtikartechnicaltask.models.Profile
import com.ragabz.ibtikartechnicaltask.ui.personimage.viewmodels.ImageViewModel
import java.io.Serializable
import javax.inject.Inject


class ImageActivity : BaseActivity() {

    private lateinit var binding: ActivityImageBinding

    @Inject
    lateinit var providerFactory: ViewModelProvidersFactory

    private val imageViewModel: ImageViewModel by lazy {
        ViewModelProvider(this, providerFactory).get(ImageViewModel::class.java)
    }

    companion object {
        const val EXTRA_PROFILE = "com.ragab.intikartask.profile_object"
        fun newIntent(context: Context, profile: Profile): Intent {
            return Intent(context, ImageActivity::class.java).apply {
                putExtra(EXTRA_PROFILE, profile as Serializable)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            com.ragabz.ibtikartechnicaltask.R.layout.activity_image
        )

        if (!checkPermissionForWriteExtertalStorage()) {
            requestPermissionForWriteExtertalStorage()
        }
        binding.lifecycleOwner = this
        getFilePathFromIntent()

        binding.imageViewModel = imageViewModel
        setupOnDownloadClickListener()
    }

    private fun getFilePathFromIntent() {
        imageViewModel.setProfile(intent.extras?.get(EXTRA_PROFILE) as Profile)
    }

    private fun setupOnDownloadClickListener() {
        binding.imageDownload.setOnClickListener { _ ->
            val request =
                DownloadManager.Request(Uri.parse(imageViewModel.observableImageUrl.get()))
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
            request.setTitle("download")
            request.setDescription("downloading...")
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_PICTURES,
                "${System.currentTimeMillis()}"
            )
            val manager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            manager.enqueue(request)
        }
    }

    fun checkPermissionForWriteExtertalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = checkSelfPermission(WRITE_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    fun requestPermissionForWriteExtertalStorage() {
        try {
            ActivityCompat.requestPermissions(
                this as Activity, arrayOf(WRITE_EXTERNAL_STORAGE),
                101
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}
