package com.ragabz.ibtikartechnicaltask.ui.persondetails.viewmodels

import com.ragabz.ibtikartechnicaltask.models.Profile
import com.ragabz.ibtikartechnicaltask.utils.Constants

class PersonImageViewModel(var profile: Profile) {

    val imageUrl: String
        get() {
            return "${Constants.IMAGE_BASE_URL}${profile.filePath}"
        }
}