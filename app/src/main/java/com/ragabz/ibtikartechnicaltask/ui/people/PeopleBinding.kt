package com.ragabz.ibtikartechnicaltask.ui.people

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.recyclerview.widget.RecyclerView
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodels.PersonViewModel

@BindingAdapter("peopleList", "loader")
fun bindPeopleList(
    recyclerView: RecyclerView,
    observablePeopleList: ObservableArrayList<PersonViewModel>,
    isLoaderNeeded: ObservableBoolean
) {
    var peopleAdapter: PeopleAdapter? = recyclerView.adapter as PeopleAdapter?
    if (peopleAdapter == null) {
        peopleAdapter = PeopleAdapter(observablePeopleList, recyclerView.context)
        recyclerView.adapter = peopleAdapter
        peopleAdapter.isLoaderNeeded = isLoaderNeeded
    }
    peopleAdapter.notifyDataSetChanged()

}