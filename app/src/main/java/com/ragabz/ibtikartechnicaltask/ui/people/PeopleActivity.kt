package com.ragabz.ibtikartechnicaltask.ui.people

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.support.v7.widget.scrollStateChanges
import com.ragabz.ibtikartechnicaltask.R
import com.ragabz.ibtikartechnicaltask.common.ViewModelProvidersFactory
import com.ragabz.ibtikartechnicaltask.common.base.BaseActivity
import com.ragabz.ibtikartechnicaltask.common.extensions.toast
import com.ragabz.ibtikartechnicaltask.common.views.ItemClickSupport
import com.ragabz.ibtikartechnicaltask.databinding.ActivityPeopleBinding
import com.ragabz.ibtikartechnicaltask.models.Person
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodel.PeopleListViewModel
import com.ragabz.ibtikartechnicaltask.ui.persondetails.PersonDetailsActivity
import com.ragabz.ibtikartechnicaltask.utils.Utils
import javax.inject.Inject

class PeopleActivity : BaseActivity(), PeopleViewModelDelegate {

    private val TAG = PeopleActivity::class.java.simpleName

    private lateinit var binding: ActivityPeopleBinding

    @Inject
    lateinit var providerFactory: ViewModelProvidersFactory

    private val peopleListViewModel: PeopleListViewModel by lazy {
        ViewModelProvider(this, providerFactory).get(PeopleListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_people)

        binding.lifecycleOwner = this

        peopleListViewModel.delegate = this
        binding.peopleListViewModel = peopleListViewModel

        initLayoutManager()
        initRecyclerViewItemDecoration()
        binding.peopleRecyclerView.itemAnimator = DefaultItemAnimator()
        setupClickListenerToPeopleRecyclerView()
        setupInfiniteScrollListener()
        Utils.runAfter(1000, { peopleListViewModel.fetchPeople(true) })
    }

    override fun onFetchError(error: String) {
        toast(error)
    }

    fun initLayoutManager() {
        val layoutManager = GridLayoutManager(this, 2)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                when (binding.peopleRecyclerView.adapter?.getItemViewType(position)) {
                    ItemType.LOADING.value -> return 2
                    ItemType.ITEM.value -> return 1
                    else -> return -1
                }
            }
        }
        binding.peopleRecyclerView.setLayoutManager(layoutManager)
    }

    fun initRecyclerViewItemDecoration() {
        binding.peopleRecyclerView.addItemDecoration(
            DividerItemDecoration(this, RecyclerView.VERTICAL)
        )
    }


    fun setupClickListenerToPeopleRecyclerView() {
        ItemClickSupport.addTo(binding.peopleRecyclerView).setOnItemClickListener(object :
            ItemClickSupport.OnItemClickListener {
            override fun onItemClicked(recyclerView: RecyclerView, position: Int, v: View) {
                val person: Person = peopleListViewModel.getPersonModelAtIndex(position)
                startActivity(PersonDetailsActivity.newIntent(this@PeopleActivity, person))
            }
        })
    }

    fun setupInfiniteScrollListener() {
        binding.peopleRecyclerView.scrollStateChanges().subscribe {
            ((binding.peopleRecyclerView.layoutManager) as? GridLayoutManager)?.let { gridLayoutManager ->
                val totalItemsCount = gridLayoutManager.itemCount
                val visibleItemsCount = gridLayoutManager.childCount
                val firstVisibleItem = gridLayoutManager.findFirstVisibleItemPosition()
                if (visibleItemsCount + firstVisibleItem >= totalItemsCount - 2) {
                    if (!peopleListViewModel.isLoading.get()) {
                        peopleListViewModel.fetchPeople(false)
                    }
                }
            }

        }
    }
}
