package com.ragabz.ibtikartechnicaltask.ui.people.viewmodel

import androidx.lifecycle.ViewModel
import com.ragabz.ibtikartechnicaltask.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PeopleViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PeopleListViewModel::class)
    abstract fun bindPeopleListViewModel(peopleListViewModel: PeopleListViewModel): ViewModel
}