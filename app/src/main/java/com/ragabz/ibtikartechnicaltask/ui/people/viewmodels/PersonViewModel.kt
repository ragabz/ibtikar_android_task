package com.ragabz.ibtikartechnicaltask.ui.people.viewmodels

import android.util.Log
import com.ragabz.ibtikartechnicaltask.models.Person
import com.ragabz.ibtikartechnicaltask.utils.Constants

class PersonViewModel constructor(var person: Person) {

    val imageUrl: String
        get() {
            Log.d("PersonViewModel", "${Constants.IMAGE_BASE_URL}${person.profilePath}")
            return "${Constants.IMAGE_BASE_URL}${person.profilePath}"
        }
}