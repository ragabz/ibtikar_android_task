package com.ragabz.ibtikartechnicaltask.ui.people

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.recyclerview.widget.RecyclerView
import com.ragabz.ibtikartechnicaltask.BR
import com.ragabz.ibtikartechnicaltask.R
import com.ragabz.ibtikartechnicaltask.common.base.BaseViewHolder
import com.ragabz.ibtikartechnicaltask.databinding.ItemPersonBinding
import com.ragabz.ibtikartechnicaltask.databinding.ItemProgressBinding
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodels.PersonViewModel

class PeopleAdapter(var itemList: ObservableArrayList<PersonViewModel>, val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var isLoaderNeeded: ObservableBoolean = ObservableBoolean(true)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            ItemType.LOADING.value -> {
                val itemProgressBinding: ItemProgressBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.item_progress,
                    parent,
                    false
                )
                return ProgressViewHolder(itemProgressBinding.root)
            }
            else -> {
                val itemMovieBinding: ItemPersonBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(context),
                    R.layout.item_person,
                    parent,
                    false
                )
                return PersonViewHolder(itemMovieBinding.root)
            }
        }
    }

    override fun getItemCount(): Int = itemList.size.plus(if (isLoaderNeeded.get()) 1 else 0)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PersonViewHolder) {
            if (position <= itemList.size -1) {
                holder.binding?.setVariable(BR.personViewModel, itemList[position])
                holder.binding?.executePendingBindings()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemList.size) ItemType.LOADING.value else ItemType.ITEM.value
    }

    internal class PersonViewHolder(itemView: View) : BaseViewHolder(itemView)
    internal class ProgressViewHolder(itemView: View) : BaseViewHolder(itemView)
}
