package com.ragabz.ibtikartechnicaltask.ui.persondetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.RecyclerView
import com.ragabz.ibtikartechnicaltask.BR
import com.ragabz.ibtikartechnicaltask.R
import com.ragabz.ibtikartechnicaltask.common.base.BaseViewHolder
import com.ragabz.ibtikartechnicaltask.databinding.ItemPersonImageBinding
import com.ragabz.ibtikartechnicaltask.ui.persondetails.viewmodels.PersonImageViewModel

class PersonImageAdapter(
    var itemList: ObservableArrayList<PersonImageViewModel>,
    val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemPersonImageBinding
                : ItemPersonImageBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.item_person_image,
            parent,
            false
        )
        return PersonViewHolder(itemPersonImageBinding.root)
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PersonViewHolder) {
            holder.binding?.setVariable(BR.personImageViewModel, itemList[position])
            holder.binding?.executePendingBindings()
        }
    }

    internal class PersonViewHolder(itemView: View) : BaseViewHolder(itemView)
}
