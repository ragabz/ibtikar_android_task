package com.ragabz.ibtikartechnicaltask.ui.people.viewmodel

import androidx.lifecycle.ViewModel
import com.ragabz.ibtikartechnicaltask.di.ViewModelKey
import com.ragabz.ibtikartechnicaltask.ui.persondetails.viewmodels.PersonDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PersonDetailsViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PersonDetailsViewModel::class)
    abstract fun bindPersonDetailsViewModel(personDetailsViewModel: PersonDetailsViewModel): ViewModel
}