package com.ragabz.ibtikartechnicaltask.ui.people

interface PeopleViewModelDelegate {
    fun onFetchError(error: String)
}