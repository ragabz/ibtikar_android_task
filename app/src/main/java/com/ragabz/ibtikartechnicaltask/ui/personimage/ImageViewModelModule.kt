package com.ragabz.ibtikartechnicaltask.ui.people.viewmodel

import androidx.lifecycle.ViewModel
import com.ragabz.ibtikartechnicaltask.di.ViewModelKey
import com.ragabz.ibtikartechnicaltask.ui.personimage.viewmodels.ImageViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ImageViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ImageViewModel::class)
    abstract fun bindPersonDetailsViewModel(imageViewModel: ImageViewModel): ViewModel
}