package com.ragabz.ibtikartechnicaltask.ui.personimage.viewmodels

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.ragabz.ibtikartechnicaltask.common.base.BaseViewModel
import com.ragabz.ibtikartechnicaltask.models.Profile
import com.ragabz.ibtikartechnicaltask.utils.Constants
import javax.inject.Inject

class ImageViewModel @Inject constructor() : BaseViewModel() {

    private lateinit var profile: Profile

    val observableImageUrl = ObservableField<String>("")
    val observableImageWidth = ObservableInt()
    val observableImageHeight = ObservableInt()

    fun setProfile(profile: Profile) {
        this.profile = profile
        observableImageUrl.set("${Constants.IMAGE_BASE_URL}${this.profile.filePath}")
        observableImageWidth.set(this.profile.width)
        observableImageHeight.set(this.profile.height)
    }
}