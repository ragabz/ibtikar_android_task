package com.ragabz.ibtikartechnicaltask.ui.persondetails.viewmodels

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.ragabz.ibtikartechnicaltask.common.base.BaseViewModel
import com.ragabz.ibtikartechnicaltask.common.helpers.InternetHelper
import com.ragabz.ibtikartechnicaltask.data.PeopleRepository
import com.ragabz.ibtikartechnicaltask.models.Person
import com.ragabz.ibtikartechnicaltask.models.Profile
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodels.PersonViewModel
import com.ragabz.ibtikartechnicaltask.utils.Constants
import io.reactivex.Observable
import javax.inject.Inject

class PersonDetailsViewModel @Inject constructor(
    private val peopleRepository: PeopleRepository
) : BaseViewModel() {

    private val TAG = PersonDetailsViewModel::class.java.simpleName

    val personImage = ObservableField<String>("")
    val personName = ObservableField<String>("")
    val adult = ObservableBoolean(false)
    val gender = ObservableInt(0)
    val knownForDepartment = ObservableField<String>("")

    val imageList = ObservableArrayList<PersonImageViewModel>()

    val progress = ObservableBoolean(true)


    private lateinit var person: Person


    fun initPerson(person: Person) {
        this.person = person
        personImage.set("${Constants.IMAGE_BASE_URL}${person.profilePath}")
        personName.set(person.name)
        adult.set(person.adult)
        gender.set(person.gender)
        knownForDepartment.set(person.knownForDepartment)
    }


    fun loadPersonImages() {
        val internetConnectionDisposable = InternetHelper.isConnected().subscribe { isConnected ->
            when (isConnected) {
                true -> getPersonImages()
                false -> handleNoInternetConnection()
            }
        }
        activeRequests.add(internetConnectionDisposable)

    }

    private fun getPersonImages() {
        val loadImagesDisposable = peopleRepository.getPersonImages(person.id)
            .subscribe({ response ->
                hideProgress()
                Observable.fromArray(response.profiles)
                    .flatMapIterable { it }
                    .map { PersonImageViewModel(it) }.toList()
                    .subscribe { singleObserver ->
                        this.imageList.addAll(
                            singleObserver
                        )
                    }
            }, {
                onLoadImagesError(it)
            })
        activeRequests.add(loadImagesDisposable)
    }


    fun onLoadImagesError(error: Throwable) {
        hideProgress()
    }

    fun handleNoInternetConnection() {
        hideProgress()
    }

    private fun hideProgress() {
        progress.set(false)
    }

    fun getProfileAtIndex(index: Int): Profile {
        return imageList.get(index).profile
    }


}