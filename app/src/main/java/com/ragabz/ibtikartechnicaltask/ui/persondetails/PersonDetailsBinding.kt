package com.ragabz.ibtikartechnicaltask.ui.persondetails

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.recyclerview.widget.RecyclerView

import com.ragabz.ibtikartechnicaltask.ui.persondetails.viewmodels.PersonImageViewModel

@BindingAdapter("adult")
fun bindAdult(
    textView: TextView,
    adult: ObservableBoolean
) {
    textView.setText(if (adult.get()) "NO" else "Yes")
}

@BindingAdapter("gender")
fun bindGender(
    textView: TextView,
    gender: ObservableInt
) {
    textView.setText(if (gender.get() != 1) "Male" else "FeMale")
}

@BindingAdapter("imagesList")
fun bindImagesList(
    recyclerView: RecyclerView,
    observaImagesList: ObservableArrayList<PersonImageViewModel>
) {
    var personImagesAdapter: PersonImageAdapter? = recyclerView.adapter as PersonImageAdapter?
    if (personImagesAdapter == null) {
        personImagesAdapter = PersonImageAdapter(observaImagesList, recyclerView.context)
        recyclerView.adapter = personImagesAdapter

    }
    personImagesAdapter.notifyDataSetChanged()

}