package com.ragabz.ibtikartechnicaltask.ui.people.viewmodel

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import com.ragabz.ibtikartechnicaltask.common.base.BaseViewModel
import com.ragabz.ibtikartechnicaltask.common.helpers.InternetHelper
import com.ragabz.ibtikartechnicaltask.data.PeopleRepository
import com.ragabz.ibtikartechnicaltask.models.Person
import com.ragabz.ibtikartechnicaltask.ui.people.PeopleViewModelDelegate
import com.ragabz.ibtikartechnicaltask.ui.people.viewmodels.PersonViewModel
import io.reactivex.Observable
import javax.inject.Inject

class PeopleListViewModel @Inject constructor(
    private val peopleRepository: PeopleRepository
) : BaseViewModel() {

    private val TAG = PeopleListViewModel::class.java.simpleName

    private val startPage = 1
    private var totalPages = 2
    private var currentPage = startPage

    var isLoaderNeeded = ObservableBoolean(true)
    var isLoading = ObservableBoolean(false)
    var delegate: PeopleViewModelDelegate? = null

    var personViewModelList = ObservableArrayList<PersonViewModel>()

    fun fetchPeople(firstTime: Boolean) {
        val internetDisposable = InternetHelper.isConnected().subscribe { isConnected ->
            when (isConnected) {
                true -> {
                    isLoading.set(true)
                    isLoaderNeeded.set(true)
                    startFetchPeople(firstTime)
                }
                false -> {
                    publishError(error = null, errorMessage = "No InternetConnection")
                }
            }
        }
        activeRequests.add(internetDisposable)


    }

    private fun startFetchPeople(firstTime: Boolean) {
        val fetchPeopleDisposable =
            peopleRepository.fetchPopularPeople(pageNumber = if (firstTime) startPage else currentPage)
                .subscribe({ response ->
                    totalPages = response.totalPages

                    Observable.fromArray(response.people)
                        .flatMapIterable { it }
                        .map { PersonViewModel(it) }.toList()
                        .subscribe { singleObserver ->
                            this.personViewModelList.addAll(
                                singleObserver
                            )
                        }

                    isLoading.set(false)
                    println("Finished Requesting Page: $currentPage")
                    currentPage++

                }, { error -> publishError(error) })
        activeRequests.add(fetchPeopleDisposable)
    }

    private fun publishError(error: Throwable?, errorMessage: String = "") {
        delegate?.onFetchError(error?.message ?: errorMessage)
        isLoaderNeeded.set(false)
        isLoading.set(false)
    }


    fun getPersonModelAtIndex(index: Int): Person {
        return personViewModelList.get(index).person
    }
}