package com.ragabz.ibtikartechnicaltask.network

import com.ragabz.ibtikartechnicaltask.models.BaseApiResponse
import com.ragabz.ibtikartechnicaltask.models.PersonImagesResponse
import com.ragabz.ibtikartechnicaltask.utils.Constants
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface PeopleApi {

    companion object {
        const val PAGE = "page"
        const val APP_KEY = "api_key"
        const val PERSON_ID = "person_id"


        const val API3_POPULAR_PEOPLE = "person/popular"
        const val API3_PERSON_IMAGES = "person/{${PERSON_ID}}/images"
    }

    @GET(API3_POPULAR_PEOPLE)
    fun getPopularPeople(
        @Query(APP_KEY) appKey: String = Constants.APP_KEY_VALUE,
        @Query(PAGE) page: Int
    ): Single<BaseApiResponse>

    @GET(API3_PERSON_IMAGES)
    fun getPersonIMages(
        @Path(PERSON_ID) personId: Int,
        @Query(APP_KEY) appKey: String = Constants.APP_KEY_VALUE
    ): Single<PersonImagesResponse>

}