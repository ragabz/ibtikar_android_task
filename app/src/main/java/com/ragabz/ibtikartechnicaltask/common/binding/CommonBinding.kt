package com.ragabz.ibtikartechnicaltask.common.binding


import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ragabz.ibtikartechnicaltask.R

@BindingAdapter(value = ["imageUrl", "imageWidth", "imageHeight"], requireAll = false)
fun setImage(imageView: ImageView, imageUrl: String, imageWidth: Int?, imageHeight: Int?) {
    val context = imageView.getContext()
    // create request options to set placeholder or error
    val options = RequestOptions()
        .placeholder(R.drawable.placeholder)
        .error(R.drawable.placeholder)

    if (imageWidth != null && imageHeight != null) {
        Glide.with(context)
            .setDefaultRequestOptions(options)
            .load(imageUrl)
            .override(imageWidth, imageHeight)
            .into(imageView)
    } else {
        Glide.with(context)
            .setDefaultRequestOptions(options)
            .load(imageUrl)
            .into(imageView)
    }
}


@BindingAdapter("android:visibility")
fun setVisibility(view: View, value: Boolean) {
    view.setVisibility(if (value) View.VISIBLE else View.GONE)
}