package com.ragabz.ibtikartechnicaltask.common.helpers

import android.util.Log
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

class InternetHelper {

    companion object {
        private val TAG = InternetHelper::class.java.simpleName

        private val TIMEOUT_MS = 500

        /**
         * Checks whether there is a connection to the internet (google DNS server)
         *
         * @return `true` if connected to internet
         */
        fun isConnected(): Single<Boolean> {
            return Single.fromCallable {
                try {
                    // Connect to Google DNS to check for connection
                    val socket = Socket()
                    val socketAddress = InetSocketAddress("8.8.8.8", 53)
                    Log.d(
                        TAG,
                        "isConnected() --> try opening a socket to socketAddress: $socketAddress"
                    )
                    socket.connect(socketAddress, TIMEOUT_MS)
                    socket.close()
                    Log.i(TAG, "isConnected() --> TRUE")
                    true
                } catch (e: IOException) {
                    Log.d(TAG, "isConnected() --> IOException --> no internet available!")
                    false
                }
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }
}
