package com.ragabz.ibtikartechnicaltask.common.base

import androidx.databinding.BaseObservable
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel: ViewModel() {

    val activeRequests: CompositeDisposable = CompositeDisposable()
}