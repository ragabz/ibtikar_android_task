package com.ragabz.ibtikartechnicaltask.common.base

import dagger.android.support.DaggerAppCompatActivity

open class BaseActivity : DaggerAppCompatActivity()