package com.ragabz.ibtikartechnicaltask.common.base


import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView


open class BaseViewHolder(view: View): RecyclerView.ViewHolder(view){
    var binding: ViewDataBinding? = null

    init {
        binding = DataBindingUtil.bind(view)
    }
}